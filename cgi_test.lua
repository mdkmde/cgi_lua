#!/usr/bin/env lua
local _FILENAME = "cgi_test.lua"
local _AUTHOR = "Matthias Diener"
local _VERSION = "2011015"

local cgi = require("cgi")

local log = nil
--[[
local log = require("log").newLog()
log:setFile("ERROR", "/tmp/cgilua.err")
log:setFile("WARN", "/tmp/cgilua.log")
log:setFile("INFO", "/tmp/cgilua.log")
log:setFile("ACCESS", "/tmp/cgilua.log")
log:setFile("DEBUG", "/tmp/cgilua.deb")
--log:setFile("REQUEST_DUMP", "/tmp/request_dump")
--]]

local cgi_env = {
  {"SCRIPT_NAME", "SCRIPT_NAME", "cgi_test.cgi"},
}
local count = 0
local server_protocol = "http" -- http|scgi

local run = function(it_cgi_ctx)
  count = count + 1
  local cgi_ctx = cgi.init(log, cgi_env) or it_cgi_ctx
--[[
  -- if page is moved send a redirect
  -- redirect(location, [301|302], [error template])
  -- Default error code is 302
  -- error template can be a file (absolute path) or string and may contain the patterns: ${STATUS_CODE} and (${LOCATION} or ${Go to: LOCATION and find out..})
  cgi_ctx:redirect("http://example.com")
  -- or return an error message
  -- httpError([error code], [new location], [error template])
  -- Default error code is 500
  cgi_ctx:httpError(404)
--]]
  -- set cookie, before sending header
  cgi_ctx:setCookie("TestCookie", "cgi_test")
  -- additional data can be set any time
  cgi_ctx:setData("TestData", "cgi_test")
  cgi_ctx:setData("TestData", "second value")
  local file_data = ""
  for _, t_v in cgi_ctx:getFileData("file_upload") do
    file_data = string.format("Got file %s with length %d and type %s<br>File (stream) available: %s but discarded.", t_v.filename, t_v.content_length, t_v.content_type, tostring(t_v.file))
  end
  -- first call of :write will send header and discard all temporary file data
  cgi_ctx:write("<html><head><title>cgi_test " .. tostring(count) .. "</title></head><body>")
  cgi_ctx:write("<h3>Hello %s</h3>", cgi_ctx:getData("name"))
  cgi_ctx:write("<h3>ENV:</h3><p>")
  cgi_ctx:write("environment is build from the given env table or by a standard env table (see cgi.lua)<br>")
  for k, v in cgi_ctx:getEnv() do
    cgi_ctx:write(k .. "=" .. v .. "<br>")
  end
  cgi_ctx:write("</p><h3>DATA:</h3><p>")
  local form_type, form_exists = cgi_ctx:getData("formtype")
  if form_exists[1] then
    cgi_ctx:write("Data was submited via %s form<br>", form_type)
  end
  cgi_ctx:write("Missing data will return an empty string: \"" .. cgi_ctx:getData("missing_data") .. "\"<br>")
  cgi_ctx:setData("additional_data", "cgi_test")
  for k, v in cgi_ctx:getData() do
    if type(v) == "string" then
      cgi_ctx:write(k .. "=" .. v .. "<br>")
    elseif type(v) == "table" then
      cgi_ctx:write(k .. "{")
      for _, tv in pairs(v) do
        cgi_ctx:write("\"" .. tv .. "\", ")
      end
      cgi_ctx:write("}<br>")
    end
  end
  cgi_ctx:write("</p><h3>File Data:</h3><p>")
  cgi_ctx:write(file_data)
  cgi_ctx:write("</p><h3>PARG:</h3><p>")
  for k, v in cgi_ctx:getPosArg() do
    cgi_ctx:write(k .. "=" .. v .. "<br>")
  end
  cgi_ctx:write("</p><h3>COOKIE:</h3><p>")
  for k, v in cgi_ctx:getCookie() do
    cgi_ctx:write(k .. "=" .. v .. "<br>")
  end
  cgi_ctx:write("</p>")
  cgi_ctx:write([[
    <h3>Test Formular POST</h3>
    <form action=']] .. cgi_ctx:getEnv("SCRIPT_NAME") .. [[/positional/arg' method='post'>
      <input type='hidden' name='formtype' value='urlencode'>
      <table><tr>
        <td>Name:</td><td><input type='text' name='name' value='%s'></td>
      </tr><tr>
        <td>Additional information:</td><td><textarea name='textarea'>%s</textarea></td>
      </tr><tr>
        <td colspan='2'><input type='reset'><input type='submit'></td>
      </tr></table>
    </form>
    <h3>Test Formular POST multipart</h3>
    <form action=']] .. cgi_ctx:getEnv("SCRIPT_NAME") .. [[/positional/arg' method='post' enctype='multipart/form-data'>
      <input type='hidden' name='formtype' value='multipart'>
      <table><tr>
        <td>Name:</td><td><input type='text' name='name' value='%s'></td>
      </tr><tr>
        <td>File:</td><td><input type='file' name='file_upload'></td>
      </tr><tr>
        <td>Additional information:</td><td><textarea name='textarea'>%s</textarea></td>
      </tr><tr>
        <td colspan='2'><input type='reset'><input type='submit'></td>
      </tr></table>
    </form>
  </body></html>
  ]], cgi_ctx:getData("name"), cgi_ctx:getData("textarea"), cgi_ctx:getData("name"), cgi_ctx:getData("textarea"))
end

if cgi_ctx then
  run(cgi_ctx)
elseif os.getenv("GATEWAY_INTERFACE") then
  run()
else
  io.write(string.format("Not a cgi .. run %s server\r\n", server_protocol))
  io.flush()
  cgi.runServer(run, log, cgi_env, {protocol = server_protocol})
end

