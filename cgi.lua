#!/usr/bin/env lua
if tonumber((string.gsub(_VERSION, "Lua ", ""))) < 5.2 then error("Require Lua 5.2"); end
--[[
https://gitlab.com/mdkmde/cgi_lua/ - matthias (at) koerpermagie.de

Copyright (c) MIT-License
Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
]]

-- BEGIN config
local DEFAULT_SERVER_ENV = {
  http = {
    document_root = "/tmp",
    host = "127.0.0.1",
    port = 8080,
    name = "localhost",
    software = "lcgi",
  },
  scgi = {
    host = "127.0.0.1",
    port = 4000,
  },
}

local DEFAULT_MAX_CONTENT_LENGTH = 1024 * 1024 * 3 -- 3 MB
local DEFAULT_MAX_CONTENT_LENGTH_PART = 1024 * 1024 -- 1 MB

local DEFAULT_RESPONSE_CONTENT_TYPE = "text/html"
local DEFAULT_COOKIE_EXPIRE = 600
local DEFAULT_ENV = {
-- CGI environment - https://tools.ietf.org/html/rfc3875
  {"AUTH_TYPE",  "AUTH_TYPE",  "0"},
  {"CONTENT_LENGTH",  "CONTENT_LENGTH", "0"},
  {"CONTENT_TYPE",  "CONTENT_TYPE", nil},
  {"GATEWAY_INTERFACE",  "GATEWAY_INTERFACE",  nil},
  {"PATH_INFO",  "PATH_INFO",  nil},
  {"PATH_TRANSLATED",  "PATH_TRANSLATED",  nil},
  {"QUERY_STRING",  "QUERY_STRING", nil},
  {"REMOTE_ADDR", "REMOTE_ADDR",  nil},
  {"REMOTE_HOST", "REMOTE_HOST",  nil},
  {"REMOTE_IDENT",  "REMOTE_IDENT", nil},
  {"REMOTE_USER",  "REMOTE_USER",  nil},
  {"REQUEST_METHOD",  "REQUEST_METHOD", nil},
  {"SCRIPT_NAME", "SCRIPT_NAME",  nil},
  {"SERVER_NAME", "SERVER_NAME",  nil},
  {"SERVER_PORT",  "SERVER_PORT",  nil},
  {"SERVER_PROTOCOL",  "SERVER_PROTOCOL",  nil},
  {"SERVER_SOFTWARE",  "SERVER_SOFTWARE",  nil},
-- HTTP/1.1
  {"HTTP_HOST",  "HTTP_HOST",  nil},
-- SCGI
  {"SCGI",  "SCGI",  nil},
-- AUTHORIZATION
  {"HTTP_AUTHORIZATION",  "HTTP_AUTHORIZATION",  nil},
-- SERVER
  {"REQUEST_URI", "REQUEST_URI",  nil},
  {"DOCUMENT_ROOT", "DOCUMENT_ROOT",  nil},
  {"DOCUMENT_URI", "DOCUMENT_URI",  nil},
  {"REMOTE_PORT",  "REMOTE_PORT",  nil},
-- PATH
  {"PATH",  "PATH",  nil},
-- HTTP Agent Header
  {"HTTP_ACCEPT",  "HTTP_ACCEPT",  nil},
  {"HTTP_ACCEPT_CHARSET",  "HTTP_ACCEPT_CHARSET",  nil},
  {"HTTP_ACCEPT_ENCODING",  "HTTP_ACCEPT_ENCODING", nil},
  {"HTTP_ACCEPT_LANGUAGE",  "HTTP_ACCEPT_LANGUAGE", nil},
  {"HTTP_CACHE_CONTROL",  "HTTP_CACHE_CONTROL", nil},
  {"HTTP_USER_AGENT", "HTTP_USER_AGENT",  nil},
  {"HTTP_TE",  "HTTP_TE",  nil},
  {"HTTP_REFERER",  "HTTP_REFERER", nil},
-- COOKIE
  {"HTTP_COOKIE",  "HTTP_COOKIE",  nil},
-- CONTENT_LENGTH LIMIT
  {"MAX_CONTENT_LENGTH",  nil,  DEFAULT_MAX_CONTENT_LENGTH},
  {"MAX_CONTENT_LENGTH_PART",  nil,  DEFAULT_MAX_CONTENT_LENGTH_PART},
}

local HTTP_10_STATUS_CODE = {
  [200] = "OK",
  [201] = "Created",
  [202] = "Accepted",
  [204] = "No Content",
  [301] = "Moved Permanently",
  [302] = "Moved Temporarily",
  [304] = "Not Modified",
  [400] = "Bad Request",
  [401] = "Unauthorized",
  [403] = "Forbidden",
  [404] = "Not Found",
  [500] = "Internal Server Error",
  [501] = "Not Implemented",
  [502] = "Bad Gateway",
}
local DEFAULT_ERROR_PAGE = "<!DOCTYPE html PUBLIC '-//W3C//DTD HTML 4.01 Transitional//EN'><html><head><title>${STATUS_CODE}</title></head><body>${STATUS_CODE} ${LOCATION}</body></html>"
local DEFAULT_REDIRECT_STATUS_CODE = 302

-- END config

-- if no logger is provided .. DROP log.LEVEL() statements
-- log levels in use are: DEBUG, INFO, WARN, ERROR and ACCESS
local nolog = function() end
local DEFAULT_LOG = setmetatable({}, {__index = function() return nolog; end})

local _M = {}
local ipairs, pairs, pcall, require, tonumber, type = ipairs, pairs, pcall, require, tonumber, type
local io = {open = io.open, read = io.read, write = io.write}
local os = {date = os.date, getenv = os.getenv, remove = os.remove, tmpname = os.tmpname}
local string = {byte = string.byte, char = string.char, find = string.find, format = string.format, gsub = string.gsub, lower = string.lower, sub = string.sub, upper = string.upper}
local table = {concat = table.concat, unpack = table.unpack}
_ENV = nil

local _NAME = ...
local t_log = DEFAULT_LOG
local t_server_env = {}
local t_supported_request_method = {GET = true, HEAD = true, POST = true}

_M.getLog = function(io_cgi_ctx)
-- returns log table
  if (type(io_cgi_ctx) == "table") then
    return io_cgi_ctx.t_log
  end
  return t_log
end

_M.setLog = function(io_cgi_ctx, it_log)
-- returns log table
  if type(io_cgi_ctx) == "table" then
    if io_cgi_ctx.b_cgi_ctx  == true then
      io_cgi_ctx.t_log = it_log or t_log
      return io_cgi_ctx.t_log
    else
      t_log = io_cgi_ctx or DEFAULT_LOG
    end
  else
    t_log = DEFAULT_LOG
  end
  return t_log
end

-- lua special char: ^$()%.[]*+-?
-- RFC 2396
-- reserved   = ";" | "/" | "?" | ":" | "@" | "&" | "=" | "+" | "$" | ","
local s_reserved = ";/%?:@&=%+%$,"
-- delims     = "<" | ">" | "#" | "%" | <">
local s_delims = "<>#%%\""
-- unreserved = alphanum | mark
local s_alphanum = "%w"
--       mark = "-" | "_" | "." | "!" | "~" | "*" | "'" | "(" | ")"
local s_mark = "%-_%.!~%*'%(%)"
local s_unreserved = s_alphanum .. s_mark
-- sometimes modified by gateways and other transport agents or delimiters
-- unwise     = "{" | "}" | "|" | "\" | "^" | "[" | "]" | "`"
local s_unwise = "{}|\\%^%[%]`"

local stripUnknownChar = function(is)
-- returns string without unknown characters
  is = string.gsub(is, "[^" .. s_reserved .. s_delims .. s_unreserved .. s_unwise .. "\r\n ]", "")
  return is
end

_M.urlEncode = function(is)
-- returns url encoded is
  is = string.gsub(is, "[" .. s_reserved .. s_delims .. "]", function(is_char)
    return string.format("%%%02X", string.byte(is_char))
  end)
  is = string.gsub(is, " ", "+")
  is = stripUnknownChar(is)
  return is
end

_M.urlDecode = function(is)
-- returns url decoded is
  is = stripUnknownChar(is)
  is = string.gsub(is, "%+", " ")
  is = string.gsub(is, "%%(%x%x)", function(is_hex)
    return string.char(tonumber(is_hex, 16))
  end)
  return is
end

_M.httpError = function(io_cgi_ctx, in_status_code, is_url, is_error_page)
-- no return value
  local log = _M.getLog(io_cgi_ctx)
  if (type(io_cgi_ctx) == "table") and (type(io_cgi_ctx.t_header) == "table") and io_cgi_ctx.t_header.b_sent then
    log.WARN("%s.httpError() header already sent", _NAME)
  elseif not(type(io_cgi_ctx) == "table") then
    is_error_page = is_url
    is_url = in_status_code
    in_status_code = io_cgi_ctx
  end
  if type(in_status_code) == "string" then
    log.DEBUG("%s.httpError() received Status-Code as string: \"%s\" .. try to convert", _NAME, in_status_code)
    in_status_code = tonumber(string.sub(in_status_code, 1, 3))
  end
  if not(type(in_status_code) == "number") or not(HTTP_10_STATUS_CODE[in_status_code]) then
    in_status_code = 500
  end
  local s_status_code = string.format("%d %s", in_status_code, HTTP_10_STATUS_CODE[in_status_code])
  if t_server_env.LCGI_SERVER_PROTOCOL == "http" then
    io.write(string.format("HTTP/1.0 %s\r\n", s_status_code))
  else
    io.write(string.format("Status: %s\r\n", s_status_code))
  end
  if type(is_url) == "string" then
    log.DEBUG("%s.httpError() Location: %s", _NAME, is_url)
    io.write("Location: " .. (string.gsub(is_url, "[\r\n]", "")) .. "\r\n")
  end
  local s_error_page = DEFAULT_ERROR_PAGE
  if type(is_error_page) == "string" and not(is_error_page == "") then
    local f_error = io.open(is_error_page)
    if f_error then
      local s_content, s_err = f_error:read("*a")
      if s_err then
        log.ERROR("%s.httpError() Cannot read file \"%s\" content: %s", _NAME, is_error_page, s_err)
      else
        s_error_page = s_content
      end
      f_error:close()
    else
      s_error_page = is_error_page
    end
  end
  local s_error_content_type = string.find(s_error_page, "<[Hh][Tt][Mm][Ll]>") and "html" or "plain"
  log.DEBUG("%s.httpError() Content-Type: text/%s", _NAME, s_error_content_type)
  io.write(string.format("Content-Type: text/%s\r\n", s_error_content_type))
  io.write("\r\n")
  io.write(string.gsub(s_error_page, "%${([^}]*)}", function(is_pattern)
    log.DEBUG("%s.httpError() found pattern to replace in error page: %s", _NAME, is_pattern)
    if is_pattern == "STATUS_CODE" then
      return s_status_code
    elseif string.find(is_pattern, "LOCATION") and is_url then
      return string.gsub(is_pattern, "LOCATION", (s_error_content_type == "html") and "<a href ='" .. is_url .. "'>" .. is_url .. "</a>" or is_url)
    end
    return ""
  end))
end

_M.redirect = function(io_cgi_ctx, is_url, in_status_code, is_error_page)
-- no return value
  local log = _M.getLog(io_cgi_ctx)
  if (type(io_cgi_ctx) == "table") and (type(io_cgi_ctx.t_header) == "table") and io_cgi_ctx.t_header.b_sent then
    log.WARN("%s.redirect() header already sent", _NAME)
  elseif type(io_cgi_ctx) == "string" then
    is_error_page = in_status_code
    in_status_code = is_url
    is_url = io_cgi_ctx
  end
  if type(in_status_code) == "string" then
    log.DEBUG("%s.redirect() received Status-Code as string: \"%s\" .. try to convert", _NAME, in_status_code)
    in_status_code = tonumber(string.sub(in_status_code, 1, 3))
  end
  if not(type(in_status_code) == "number") or (not(in_status_code == 301) and not(in_status_code == 302)) then
    in_status_code = DEFAULT_REDIRECT_STATUS_CODE
  end
  log.DEBUG("%s.redirect() Status-Code: %d %s", _NAME, in_status_code, HTTP_10_STATUS_CODE[in_status_code])
  is_url = is_url or "/"
  log.INFO("%s.redirect() Location: %s", _NAME, is_url)
  _M.httpError(in_status_code, is_url, is_error_page)
end

local setHeader = function(io_cgi_ctx, i_header, ib_replace)
-- returns position 0 < n on success
  local log = _M.getLog(io_cgi_ctx)
  if not(type(io_cgi_ctx) == "table") or not(type(io_cgi_ctx.t_header) == "table") then
    log.ERROR("%s.write() no cgi context", _NAME)
    return
  end
  if io_cgi_ctx.t_header.b_sent then
    log.WARN("%s.setHeader() header already sent", _NAME)
    return
  end
  if type(i_header) == "string" then
    i_header = {i_header}
  end
  if ib_replace then
    io_cgi_ctx.t_header = {"Content-Type: " .. DEFAULT_RESPONSE_CONTENT_TYPE}
  end
  for _, s_h in ipairs(i_header) do
    if (type(s_h) == "string") and string.find(s_h, ":") then
      s_h = string.gsub(s_h, "[\r\n]", "")
      if string.find(string.lower(s_h), "content-type:", 1, true) == 1 then
        io_cgi_ctx.t_header[1] = s_h
      else
        io_cgi_ctx.t_header[(#io_cgi_ctx.t_header + 1)] = s_h
      end
      log.DEBUG("%s.setHeader() %d. %s", _NAME, #io_cgi_ctx.t_header, s_h)
    else
      log.WARN("%s.setHeader() invalid header", _NAME)
    end
  end
  return #io_cgi_ctx.t_header
end

local setContentType = function(io_cgi_ctx, is_content_type)
-- returns position 0 < n on success
  return setHeader(io_cgi_ctx, "Content-Type: " .. is_content_type or DEFAULT_RESPONSE_CONTENT_TYPE)
end

local setCookie = function(io_cgi_ctx, is_name, is_value)
-- returns position 0 < n on success
  local log = _M.getLog(io_cgi_ctx)
  if not(type(io_cgi_ctx) == "table") or not(type(io_cgi_ctx.t_cookie) == "table") then
    log.WARN("%s.setCookie() no cgi context", _NAME)
    return
  end
  -- name = value[; Comment = ""][; Domain = "\..*"][; Max-Age = delta-seconds][; Path = ".*"][; Secure]; Version = 1
  local t_set_cookie = {
    [1] = "",
    [2] = false,
    [3] = "",
    [4] = io_cgi_ctx.t_set_cookie._DEFAULT_COOKIE_DOMAIN,
    [5] = string.format("; Max-Age = %d", DEFAULT_COOKIE_EXPIRE),
    [6] = "; Path = \"" .. io_cgi_ctx.t_set_cookie._DEFAULT_COOKIE_PATH .. "\"",
    [7] = "",
    [8] = "; Version = 1"
  }
  local t_cookie = {name = is_name, value = is_value}
  if type(is_name) == "table" then
    t_cookie = is_name
  end
  for s_k, s_v in pairs(t_cookie) do
    log.DEBUG("%s.setCookie() %s = \"%s\"", _NAME, s_k, s_v)
    s_k = string.lower(s_k)
    if s_k == "name" then
      t_set_cookie[1] = s_v .. " = "
    elseif s_k == "value" then
      t_set_cookie[2] = "\"" .. s_v .. "\""
    elseif s_k == "comment" then
      t_set_cookie[3] = "; Comment = \"" .. s_v .. "\""
    elseif s_k == "domain" then
      t_set_cookie[4] = s_v
    elseif s_k == "max-age" then
      t_set_cookie[5] = "; Max-Age = " .. ((type(s_v) == "number") and string.format("%d", s_v) or s_v)
    elseif s_k == "path" then
      t_set_cookie[6] = "; Path = \"" .. s_v .. "\""
    elseif s_k == "secure" then
      t_set_cookie[7] =  "; Secure"
    end
  end
  if not(t_set_cookie[2]) then
    log.DEBUG("%s.setCookie() value does not exist; destroy cookie", _NAME)
    t_set_cookie[2] = ""
    t_set_cookie[5] = "; Max-Age = 0"
  end
  t_set_cookie[4] = "; Domain = \"" .. ((string.sub(t_set_cookie[4], 1, 1) == ".") and t_set_cookie[4] or ("." .. t_set_cookie[4])) .. "\""
  local s_cookie = table.concat(t_set_cookie)
  log.DEBUG("%s.setCookie(%s)", _NAME, s_cookie)
  return setHeader(io_cgi_ctx, "Set-Cookie: " .. s_cookie)
end

local parseHttpHeader = function(i_http_server_sock_io)
-- returns scgi env table
  local log = _M.getLog()
  local t_cgi_env = {table.unpack(t_server_env)}
  local s_in, s_err = i_http_server_sock_io:receive("*l")
  if not(s_err) then
    local _, n_err = string.gsub(s_in, "^([^ ]+) ([^? ]+)(%??)([^ ]*) [hH][Tt][Tt][Pp]/1.([01])$", function(is_request_method, is_path, is_query, is_query_string, is_server_protocol)
      is_request_method = string.upper(is_request_method)
      if t_supported_request_method[is_request_method] then
        local s_uri = string.format("%s%s%s", is_path, is_query, is_query_string)
        t_cgi_env.DOCUMENT_URI = s_uri
        t_cgi_env.QUERY_STRING = is_query == "?" and is_query_string or ""
        t_cgi_env.REMOTE_ADDR = i_http_server_sock_io:getpeername()
        t_cgi_env.REQUEST_METHOD = is_request_method
        t_cgi_env.REQUEST_URI = s_uri
        t_cgi_env.SCRIPT_NAME = is_path
        t_cgi_env.SERVER_PROTOCOL = string.format("HTTP/1.%s", is_server_protocol)
        log.DEBUG("%s.parseHttpHeader() new http request: %s", _NAME, s_in)
      else
        s_err = string.format("request method not supported: %s", is_request_method)
      end
    end)
    if not(n_err == 1) then
      s_err = string.format("invalid http request: %s", s_in)
    end
    s_in, s_err = i_http_server_sock_io:receive("*l")
  end
  while not(s_in == "") and not(s_err) do
    string.gsub(s_in, "^([^:]+) *: *(.*)$", function(s_header_key, s_header_value)
      local s_env_key = string.gsub(string.upper(s_header_key), "-", "_")
      if string.find(s_env_key, "^[A-Z_]+$") then
        s_env_key = t_server_env.t_trusted_header[s_env_key] and s_env_key or string.format("HTTP_%s", s_env_key)
        t_cgi_env[s_env_key] = s_header_value
        log.DEBUG("%s.parseHttpHeader() %s -> %s = \"%s\"", _NAME, s_header_key, s_env_key, s_header_value)
      else
        log.DEBUG("%s.parseHttpHeader() invalid header key: %s (= \"%s\") - DROP", _NAME, s_header_key, s_header_value)
      end
    end)
    s_in, s_err = i_http_server_sock_io:receive("*l")
  end
  if (t_cgi_env.SERVER_PROTOCOL == "HTTP/1.1") and not(t_cgi_env.HTTP_HOST) then
    s_err = "invalid HTTP/1.1 request - no host"
  end
  if s_err then
    log.ERROR("%s.parseHttpHeader() %s", _NAME, s_err)
    return nil, s_err
  end
  return t_cgi_env
end

local parseScgiHeader = function(i_scgi_server_sock_io)
-- returns scgi env table
  local log = _M.getLog()
  local t_cgi_env = {}
  local s_header_len = ""
  local s_in, s_err = i_scgi_server_sock_io:receive(1)
  while not(s_in == ":") and not(s_err) do
    s_header_len = s_header_len .. s_in
    s_in, s_err = i_scgi_server_sock_io:receive(1)
  end
  if s_err then
    log.ERROR("%s.parseScgiHeader() scgi_server_sock_io: %s", _NAME, s_err)
    return nil, s_err
  end
  s_in, s_err = i_scgi_server_sock_io:receive(tonumber(s_header_len) or 0)
  if s_err then
    log.ERROR("%s.parseScgiHeader() scgi_server_sock_io: %s", _NAME, s_err)
    return nil, s_err
  end
  string.gsub(s_in, "([^%z]+)%z([^%z]+)%z", function(k, v) t_cgi_env[string.upper(k)] = v; end)
  if not(i_scgi_server_sock_io:receive(1) == ",") then
    log.WARN("%s.parseScgiHeader() incorrect netstring in scgi header", _NAME)
  end
  return t_cgi_env
end

local setData = function(io_cgi_ctx, is_key, is_value, ib_new)
-- returns position 0 < n on success
  local log = _M.getLog(io_cgi_ctx)
  if not(type(io_cgi_ctx) == "table") or not(type(io_cgi_ctx.t_data) == "table") then
    log.ERROR("%s.setData() no cgi context", _NAME)
    return
  end
  if not(type(is_key) == "string") then
    log.ERROR("%s.setData() key is not a string", _NAME)
    return
  end
  if not(type(is_value) == "string") then
    log.ERROR("%s.setData() value is not a string", _NAME)
    return
  end
  if (type(io_cgi_ctx.t_data[is_key]) == "nil") or ib_new then
    io_cgi_ctx.t_data[is_key] = {is_value}
    log.DEBUG("%s.setData() %s = {\"%s\"}", _NAME, is_key, is_value)
  elseif type(io_cgi_ctx.t_data[is_key]) == "table" then
    io_cgi_ctx.t_data[is_key][(#io_cgi_ctx.t_data[is_key] + 1)] = is_value
    log.DEBUG("%s.setData() %s[%d] = \"%s\"", _NAME, is_key, #io_cgi_ctx.t_data[is_key], is_value)
  end
  return #io_cgi_ctx.t_data[is_key]
end

local parseQueryString = function(io_cgi_ctx, is_query_string)
-- no return value
  local log = _M.getLog(io_cgi_ctx)
  if not(io_cgi_ctx) or not(is_query_string) then
    log.ERROR("%s.parseQueryString() incorrect function call", _NAME)
    return
  end
  if string.find(is_query_string, "&") == 1 then
    is_query_string = string.sub(is_query_string, 2)
  end
  string.gsub(is_query_string, "([^&=]+)=?([^&]*)&?", function(is_key, is_value)
    log.DEBUG("%s.parseQueryString() QUERY_STRING: %s = \"%s\"", _NAME, is_key, is_value)
    is_key = stripUnknownChar(is_key)
    is_value = _M.urlDecode(is_value)
    setData(io_cgi_ctx, is_key, is_value)
  end)
end

local readMultipartFormData = function(io_cgi_ctx, in_content_start, it_content_header, if_request_dump)
-- no return value
  local log = _M.getLog(io_cgi_ctx)
  local s_boundary = it_content_header.s_boundary or "\r\n"
  local o_destination = it_content_header.f_tmp or {write = function(it_self, is_content) it_self[(#it_self + 1)] = is_content; end}
  local s_buf = io.read(1)
  local n_content_read = in_content_start + 1
  while s_buf do
    if string.find(s_boundary, s_buf, 1, true) == 1 then
      if #s_buf < #s_boundary then
        s_buf = s_buf .. io.read(1)
      else
        it_content_header.n_content_length = n_content_read - in_content_start - #s_boundary
        if not(s_boundary == "\r\n") then
          local s_end = io.read(2)
          s_buf = s_buf .. s_end
          n_content_read = n_content_read + 2
          if (s_end == "--") then
            s_end = io.read(2)
            s_buf = s_buf .. s_end
            n_content_read = n_content_read + 2
            log.DEBUG("%s.readMultipartFormData() end of multipart/form-data", _NAME)
          end
          if not(s_end == "\r\n") then
            log.ERROR("%s.readMultipartFormData() corrupt multipart/form-data", _NAME)
          end
        end
        if if_request_dump then
          if_request_dump:write(s_buf)
        end
        if type(o_destination) == "table" then
          it_content_header.s_value = _M.urlDecode(table.concat(o_destination))
        else
          it_content_header.s_value = string.format("%s, content_length = %d}", it_content_header.s_value, it_content_header.n_content_length)
          o_destination:close()
        end
        log.DEBUG("%s.readMultipartFormData() content (%d) = \"%s\"", _NAME, it_content_header.n_content_length, it_content_header.s_value)
        return n_content_read, it_content_header
      end
    else
      if if_request_dump then
        if_request_dump:write(s_buf)
      end
      if (n_content_read - in_content_start) < io_cgi_ctx.n_max_content_length_part then
        o_destination:write(s_buf)
      end
      s_buf = io.read(1)
    end
    n_content_read = n_content_read + 1
  end
  log.ERROR("%s.readMultipartFormData() %d", _NAME, n_content_read)
  return -1
end

local parseMultipartFormData = function(io_cgi_ctx, is_content_type, in_content_length, if_request_dump)
-- no return value
  local log = _M.getLog(io_cgi_ctx)
  local s_boundary = "\r\n--" .. string.gsub(is_content_type, ".*[Bb][Oo][Uu][Nn][Dd][Aa][Rr][Yy] *= *\"?([^\"\r\n]*)\"?.*", "%1")
  log.DEBUG("%s.parseMultipartFormData() set boundary = \"%s\"", _NAME, s_boundary)
  local n_content_read = 0
  local t_content_header = {}
  while n_content_read < in_content_length do
    n_content_read, t_content_header = readMultipartFormData(io_cgi_ctx, n_content_read, t_content_header, if_request_dump)
    log.DEBUG("%s.parseMultipartFormData() %d/%d", _NAME, n_content_read, in_content_length)
    if n_content_read == -1 then
      break
    end
    if not(t_content_header.s_key) then
      if (t_content_header.s_value == "") then
        if t_content_header["is_content-disposition"] then
          string.gsub(t_content_header["is_content-disposition"], " *([^ =;]+) *=? *\"?([^\";\r]*)\"? *;?\r?", function(s_k, s_v)
            log.DEBUG("%s.parseMultipartFormData() content-disposition: %s = \"%s\"", _NAME, s_k, s_v)
            s_k = string.lower(s_k)
            if s_k == "name" then
              t_content_header.s_key = s_v
            elseif (s_k == "filename") and not(s_v == "") then
              t_content_header.s_tmpname = os.tmpname()
              if not(io_cgi_ctx.t_tmpname) then
                io_cgi_ctx.t_tmpname = {}
              end
              io_cgi_ctx.t_tmpname[(#io_cgi_ctx.t_tmpname + 1)] = t_content_header.s_tmpname
              t_content_header.f_tmp = io.open(t_content_header.s_tmpname, "w")
              t_content_header.s_value = string.format("file://%s {filename = \"%s\", content_type = \"%s\"", t_content_header.s_tmpname, s_v, ((type(t_content_header["is_content-type"]) == "string") and t_content_header["is_content-type"] or ""))
              log.DEBUG("%s.parseMultipartFormData() open file = \"%s\"", _NAME, t_content_header.s_value)
            end
          end)
        end
        t_content_header.s_boundary = s_boundary
      else
        string.gsub(t_content_header.s_value, "([^ :]+) *: *([^\r]*)\r?", function(s_k, s_v)
          log.DEBUG("%s.parseMultipartFormData() Header: %s = \"%s\"", _NAME, s_k, s_v)
          t_content_header["is_" .. string.lower(s_k)] = s_v
        end)
      end
    else
      t_content_header.s_key = stripUnknownChar(t_content_header.s_key)
      if t_content_header.n_content_length > io_cgi_ctx.n_max_content_length_part then
        local s_err = string.format("content of \"%s\" > %d -> skipped", string.gsub(t_content_header.s_value, "^[^{]*{", "{"), io_cgi_ctx.n_max_content_length_part)
        log.WARN("%s.parseMultipartFormData() %s", _NAME, s_err)
        setData(io_cgi_ctx, t_content_header.s_key, s_err)
      else
        log.DEBUG("%s.parseMultipartFormData() Body: %s = \"%s\"", _NAME, t_content_header.s_key, t_content_header.s_value)
        setData(io_cgi_ctx, t_content_header.s_key, t_content_header.s_value)
      end
      t_content_header = {}
    end
  end
end

local parsePositionalArg = function(io_cgi_ctx, is_path_info)
-- no return value
  local log = _M.getLog(io_cgi_ctx)
  string.gsub(is_path_info, "([^/]+)/?", function(is_parg)
    is_parg = _M.urlDecode(is_parg)
    io_cgi_ctx.t_parg[(#io_cgi_ctx.t_parg + 1)] = is_parg
    log.DEBUG("%s.parsePositionalArg() %d = \"%s\"", _NAME, #io_cgi_ctx.t_parg, is_parg)
  end)
end

local parseCookie = function(io_cgi_ctx, is_http_cookie)
-- no return value
  local log = _M.getLog(io_cgi_ctx)
  string.gsub(is_http_cookie, "([^= ]+) *= *\"?([^;\"]*)\"?;? *", function(is_key, is_value)
    io_cgi_ctx.t_cookie[is_key] = is_value or ""
    log.DEBUG("%s.parseCookie() %s = \"%s\"", _NAME, is_key, is_value)
  end)
end

local parseEnv = function(io_cgi_ctx, i_cgi_getenv, it_env)
-- no return value
  local log = _M.getLog(io_cgi_ctx)
  for _, t_env_data in ipairs(it_env) do
    if t_env_data[1] and not(t_env_data[1] == "") then
      io_cgi_ctx.t_env[t_env_data[1]] = t_env_data[2] and i_cgi_getenv(t_env_data[2]) or t_env_data[3]
      log.DEBUG("%s.parseEnv() %s = \"%s\"", _NAME, t_env_data[1], io_cgi_ctx.t_env[t_env_data[1]] or "")
    end
  end
end

local getData = function(io_cgi_ctx, is_key, is_seperator)
-- returns is_key and (is_seperator and value_string_list or (value[1] value_table)) or (is_separator and query_string or iterator on data table)
  local log = _M.getLog(io_cgi_ctx)
  if not(type(io_cgi_ctx) == "table") or not(type(io_cgi_ctx.t_data) == "table") then
    log.WARN("%s.getData() no cgi context", _NAME)
    return
  end
  if is_key then
    if not(type(is_key) == "string") then
      log.WARN("%s.getData() invalid key: no string", _NAME)
      return "", {}
    end
    if io_cgi_ctx.t_data[is_key] then
      if type(is_seperator) == "string" then
        return table.concat(io_cgi_ctx.t_data[is_key], is_seperator), io_cgi_ctx.t_data[is_key]
      end
      return io_cgi_ctx.t_data[is_key][1], io_cgi_ctx.t_data[is_key]
    end
    log.DEBUG("%s.getData() no key", _NAME)
    return "", {}
  end
  if is_seperator then
    local qs = ""
    for s_k, t_v in pairs(io_cgi_ctx.t_data) do
      for _, s_v in ipairs(t_v) do
        qs = qs .. "&" .. s_k .. "=" .. s_v
      end
    end
    return "?" .. string.sub(qs, 2)
  end
  return pairs(io_cgi_ctx.t_data)
end

local getFileData = function(io_cgi_ctx, is_key)
-- returns iterator on file table (file (r), filename, content_type, content_length)
  local log = _M.getLog(io_cgi_ctx)
  local _, t_data = getData(io_cgi_ctx, is_key)
  local t_file = {}
  for _, s_v in ipairs(t_data) do
    string.gsub(s_v, "file://([^ ]+) {filename = \"([^\"]+)\", content_type = \"([^\"]*)\", content_length = ([0-9]+)}", function(is_tmpname, is_filename, is_content_type, is_content_length)
      log.DEBUG("%s.getFileData() open %s (%s, %d)", _NAME, is_filename, is_content_type, is_content_length)
      t_file[(#t_file + 1)] = {file = io.open(is_tmpname), filename = is_filename, content_type = is_content_type, content_length = (tonumber(is_content_length) or 0)}
    end)
  end
  return ipairs(t_file)
end

local getPosArg = function(io_cgi_ctx, in_key, in_last)
-- returns in_last and "in_key/../in_last" or (in_key == "#") and size or in_key and (value or next value or "") or iterator on parg table
  local log = _M.getLog(io_cgi_ctx)
  if not(type(io_cgi_ctx) == "table") or not(type(io_cgi_ctx.t_parg) == "table") then
    log.WARN("%s.getPosArg() no cgi context", _NAME)
    return
  end
  if in_last then
    if type(in_last) == "string" then
      in_last = tonumber(in_last)
    end
    if type(in_last) == "number" then
      if type(in_key) == "string" then
        in_key = tonumber(in_key)
      end
      if type(in_key) == "number" then
        if in_last < 0 then
          in_last = in_last + (#io_cgi_ctx.t_parg + 1)
        end
        return table.concat(io_cgi_ctx.t_parg, "/", in_key, in_last)
      end
    end
    return ""
  end
  if in_key then
    if type(in_key) == "number" then
      return io_cgi_ctx.t_parg[in_key] or ""
    elseif type(in_key) == "string" then
      local pos = tonumber(in_key)
      if pos then
        local s_value = io_cgi_ctx.t_parg[pos] or ""
        log.DEBUG("%s.getPosArg() %s = \"%s\"", _NAME, in_key, s_value)
        return s_value
      end
      if in_key == "#" then
        return #io_cgi_ctx.t_parg
      end
      for n_k, s_v in ipairs(io_cgi_ctx.t_parg) do
        if s_v == in_key then
          local s_value = io_cgi_ctx.t_parg[(n_k + 1)] or ""
          log.DEBUG("%s.getPosArg() \"%s\" -> \"%s\"", _NAME, in_key, s_value)
          return s_value
        end
      end
    end
    return ""
  end
  return ipairs(io_cgi_ctx.t_parg)
end

local getCookie = function(io_cgi_ctx, is_key)
-- returns is_key and (value or "") or iterator on cookie table
  local log = _M.getLog(io_cgi_ctx)
  if not(type(io_cgi_ctx) == "table") or not(type(io_cgi_ctx.t_cookie) == "table") then
    log.WARN("%s.getCookie() no cgi context", _NAME)
    return
  end
  if is_key then
    if not(type(is_key) == "string") then
      log.WARN("%s.getCookie() invalid key: no string", _NAME)
      return ""
    end
    local s_value = io_cgi_ctx.t_cookie[is_key] or ""
    log.DEBUG("%s.getCookie() %s = \"%s\"", _NAME, is_key, s_value)
    return s_value
  end
  return pairs(io_cgi_ctx.t_cookie)
end

local getEnv = function(io_cgi_ctx, is_key)
-- returns is_key and (value or "") or iterator on env table
  local log = _M.getLog(io_cgi_ctx)
  if not(type(io_cgi_ctx) == "table") or not(type(io_cgi_ctx.t_env) == "table") then
    log.WARN("%s.getEnv() no cgi context", _NAME)
    return
  end
  if is_key then
    if not(type(is_key) == "string") then
      log.WARN("%s.getEnv() invalid key: no string", _NAME)
      return ""
    end
    local s_value = io_cgi_ctx.t_env[is_key] or ""
    log.DEBUG("%s.getEnv() %s = \"%s\"", _NAME, is_key, s_value)
    return s_value
  end
  return pairs(io_cgi_ctx.t_env)
end

local write = function(io_cgi_ctx, is_content, ...)
-- no return value
  local log = _M.getLog(io_cgi_ctx)
  if not(type(io_cgi_ctx) == "table") or not(type(io_cgi_ctx.t_header) == "table") then
    log.WARN("%s.write() no cgi context", _NAME)
    return
  end
  if not(io_cgi_ctx.t_header.b_sent) then
    -- cleanup MultipartFormData tmp file
    if io_cgi_ctx.t_tmpname then
      for _, t_v in ipairs(io_cgi_ctx.t_tmpname) do
        os.remove(t_v)
      end
      io_cgi_ctx.t_tmpname = nil
      log.DEBUG("%s.write() tmp files removed", _NAME)
    end
    if t_server_env.LCGI_SERVER_PROTOCOL == "http" then
      io.write(string.format("%s 200 OK\r\n", io_cgi_ctx.t_env.SERVER_PROTOCOL))
    elseif t_server_env.LCGI_SERVER_PROTOCOL == "scgi" then
      io.write("Status: 200 OK\r\n")
    end
    io.write(table.concat(io_cgi_ctx.t_header, "\r\n"))
    io.write("\r\n\r\n")
    io_cgi_ctx.t_header.b_sent = true
    log.DEBUG("%s.write() headers sent", _NAME)
  end
  io.write(#{...} > 0 and string.format(is_content, ...) or is_content)
end

local updateScriptName = function(io_cgi_ctx, is_script_name)
-- no return value
  local log = _M.getLog(io_cgi_ctx)
  if not(type(io_cgi_ctx) == "table") or not(type(io_cgi_ctx.t_env) == "table") then
    log.WARN("%s.updateScriptName() no cgi context", _NAME)
    return
  end
  local s_path_info = string.gsub(string.gsub(getEnv(io_cgi_ctx, "REQUEST_URI"), "%?.*", ""), string.format("^%s", is_script_name), "")
  io_cgi_ctx.t_env.SCRIPT_NAME = is_script_name
  io_cgi_ctx.t_env.PATH_INFO = s_path_info
  log.DEBUG("%s.updateScriptName() SCRIPT_NAME: %s, PATH_INFO: %s", _NAME, is_script_name, s_path_info)
  io_cgi_ctx.t_parg = {}
  parsePositionalArg(io_cgi_ctx, s_path_info)
end

local t_parse_header = {http = parseHttpHeader, scgi = parseScgiHeader}
_M.init = function(it_log, it_env, i_server_sock_io, is_replay_filename)
-- returns cgi_ctx object
  local log = _M.setLog(it_log)
  local cgi_getenv = (function(is_env, is_default_value) return (os.getenv(is_env) or is_default_value); end)
  if i_server_sock_io then
    local t_cgi_env, s_err = t_parse_header[t_server_env.LCGI_SERVER_PROTOCOL](i_server_sock_io)
    if s_err then
      log.ERROR("%s.init() %s", _NAME, s_err)
      return nil, s_err
    end
    io.read = (function(i) return i_server_sock_io:receive(i); end)
    io.write = (function(i) return i_server_sock_io:send(i); end)
    cgi_getenv = (function(is_env, is_default_value) return (t_cgi_env[is_env] or is_default_value); end)
  elseif is_replay_filename then
    local f_replay = io.open(is_replay_filename, "r")
    local t_replay_env = {}
    local s_line = f_replay:read("*l")
    while not(s_line == "") and not(s_line == "\r") do
      string.gsub(s_line, "([^=]+)=([^\r]*)", function(s_k, s_v) t_replay_env[s_k] = s_v; end)
      s_line = f_replay:read("*l")
    end
    io.read = (function(i) return f_replay:read(i); end)
    cgi_getenv = (function(is_env, is_default_value) return (t_replay_env[is_env] or is_default_value); end)
  end

  local o_cgi_ctx = {
    b_cgi_ctx = true,
    t_log = log,
    t_header = {"Content-Type: " .. DEFAULT_RESPONSE_CONTENT_TYPE},
    t_data = {},
    t_parg = {},
    t_cookie = {},
    t_env = {},
    setLog = _M.setLog,
    getLog = _M.getLog,
    httpError = _M.httpError,
    redirect = _M.redirect,
    setHeader = setHeader,
    setContentType = setContentType,
    setCookie = setCookie,
    setData = setData,
    getData = getData,
    getFileData = getFileData,
    getPosArg = getPosArg,
    getCookie = getCookie,
    updateScriptName = updateScriptName,
    getEnv = getEnv,
    write = write,
  }
  local f_request_dump = nil
  if log:getFile("REQUEST_DUMP") and not(is_replay_filename) then
    f_request_dump = io.open(log:getFile("REQUEST_DUMP"), "w")
    for _, s_env_key in ipairs(DEFAULT_ENV) do
      f_request_dump:write(string.format("%s=%s\r\n", s_env_key[2], cgi_getenv(s_env_key[2], "")))
    end
    f_request_dump:write("\r\n")
  end
  local REQUEST_METHOD = string.upper(cgi_getenv("REQUEST_METHOD", ""))
  log.DEBUG("%s.init() REQUEST_METHOD: %s", _NAME, REQUEST_METHOD)
  local QUERY_STRING = cgi_getenv("QUERY_STRING")
  if QUERY_STRING then
    log.DEBUG("%s.init() QUERY_STRING: %s", _NAME, QUERY_STRING)
    parseQueryString(o_cgi_ctx, QUERY_STRING)
  end
  parseEnv(o_cgi_ctx, cgi_getenv, (it_env or DEFAULT_ENV))
  if REQUEST_METHOD == "POST" then
    o_cgi_ctx.n_max_content_length_part = tonumber(getEnv(o_cgi_ctx, "MAX_CONTENT_LENGTH_PART")) or DEFAULT_MAX_CONTENT_LENGTH_PART
    o_cgi_ctx.n_max_content_length = tonumber(getEnv(o_cgi_ctx, "MAX_CONTENT_LENGTH")) or DEFAULT_MAX_CONTENT_LENGTH
    local CONTENT_TYPE = cgi_getenv("CONTENT_TYPE", "")
    local CONTENT_LENGTH = tonumber(cgi_getenv("CONTENT_LENGTH")) or 0
    if (0 < CONTENT_LENGTH) and (CONTENT_LENGTH < o_cgi_ctx.n_max_content_length) then
      if string.lower(CONTENT_TYPE) == "application/x-www-form-urlencoded" then
          local s_post_data, s_err = io.read(CONTENT_LENGTH)
          if s_err then
            log.ERROR("%s.init() Cannot read POST content: %s", _NAME, s_err)
            return nil, s_err
          end
          if f_request_dump then
            f_request_dump:write(s_post_data)
          end
          parseQueryString(o_cgi_ctx, s_post_data)
      elseif string.find(string.lower(CONTENT_TYPE), "multipart/form-data", 1, true) == 1 then
        parseMultipartFormData(o_cgi_ctx, CONTENT_TYPE, CONTENT_LENGTH, f_request_dump)
      else
        local s_err = string.format("Unsupported CONTENT_TYPE: %s", CONTENT_TYPE)
        log.ERROR("%s.init() %s", _NAME, s_err)
        return nil, s_err
      end
    else
      local s_err = string.format("No data, invalid CONTENT_LENGTH: %d (max: %d)", CONTENT_LENGTH, o_cgi_ctx.n_max_content_length)
      log.ERROR("%s.init() %s", _NAME, s_err)
      return nil, s_err
    end
  elseif not(t_supported_request_method[REQUEST_METHOD]) then
    local s_err = string.format("Unsupported REQUEST_METHOD: %s", REQUEST_METHOD)
    log.ERROR("%s.init() %s", _NAME, s_err)
    return nil, s_err
  end
  if f_request_dump then
    f_request_dump:close()
  end
  parsePositionalArg(o_cgi_ctx, cgi_getenv("PATH_INFO", ""))
  parseCookie(o_cgi_ctx, cgi_getenv("HTTP_COOKIE", ""))
  o_cgi_ctx.t_set_cookie = {
    _DEFAULT_COOKIE_DOMAIN = cgi_getenv("SERVER_NAME", ""),
    _DEFAULT_COOKIE_PATH = string.gsub(cgi_getenv("SCRIPT_NAME", "/"), "[^/]*$", ""),
  }
  if log.ACCESS then
    local REQUEST_URI = cgi_getenv("REQUEST_URI", "/")
    local REMOTE_ADDR = cgi_getenv("REMOTE_ADDR", "0.0.0.0")
    local SERVER_NAME = cgi_getenv("SERVER_NAME", "0.0.0.0")
    local s_request_time = os.date("%Y%m%dT%H%M%S")
    if (REQUEST_METHOD == "POST") then
      log.ACCESS("%s %s: %s -> %s%s%s", s_request_time, REQUEST_METHOD, REMOTE_ADDR, SERVER_NAME, REQUEST_URI, (log:getFile("ACCESS") and getData(o_cgi_ctx, false, true) or ""))
    else
      log.ACCESS("%s %s: %s -> %s%s", s_request_time, REQUEST_METHOD, REMOTE_ADDR, SERVER_NAME, REQUEST_URI)
    end
  end
  return o_cgi_ctx
end

_M.runServer = function(i_callback, it_log, it_env, it_server, is_protocol)
-- no return value
  local log = _M.setLog(it_log)
  it_server = it_server or {protocol = is_protocol}
  it_server.protocol = it_server.protocol or is_protocol
  if not(t_parse_header[it_server.protocol]) then
    log.ERROR("%s.runServer() invalid cgi lua server protocol: %s", _NAME, it_server.protocol or "nil")
    return nil, string.format("invalid cgi lua server protocol: %s", it_server.protocol or "nil")
  end
  if not(type(i_callback) == "function") then
    log.ERROR("%s.runServer() missing required callback function", _NAME)
    return nil, "missing required callback function"
  end
  for s_key, s_value  in pairs(DEFAULT_SERVER_ENV[it_server.protocol]) do
    t_server_env[string.format("SERVER_%s", string.upper(s_key))] = it_server[s_key] or s_value
  end
  t_server_env.LCGI_SERVER_PROTOCOL = it_server.protocol
  t_server_env.t_trusted_header = it_server.trusted_header
  local socket = require("socket")
  local sbind, s_err = socket.bind((it_server.host or t_server_env.SERVER_HOST), (it_server.port or t_server_env.SERVER_PORT))
  if s_err then
    log.ERROR("%s.runServer() Cannot bind socket: %s", _NAME, s_err)
    return nil, string.format("Cannot bind socket: %s", s_err)
  end
  -- server calls init / client receives on second call same context
  local cgi_init = _M.init
  local o_cgi_ctx = nil
  local init = function() return o_cgi_ctx; end
  local loop_pid_file = true
  if it_server.pid_file_name then
    loop_pid_file = io.open(it_server.pid_file_name)
  end
  log.DEBUG("%s.runServer() start lcgi server for protocol: %s", _NAME, it_server.protocol)
  while loop_pid_file do
    if it_server.pid_file_name then
      loop_pid_file:close()
    end
    sbind:settimeout(1)
    local server_sock_io
    server_sock_io, s_err = sbind:accept()
    if s_err then
      if not(s_err == "timeout") then
        log.ERROR("%s.runServer() server_sock_io: %s", _NAME, s_err)
      end
    else
      o_cgi_ctx = cgi_init(log, it_env, server_sock_io)
      if o_cgi_ctx then
        -- client script error should not crash scgi server
        local b_success
        b_success, s_err = pcall(i_callback, init())
        if not(b_success) then
          log.ERROR("%s.runServer() cgi call: %s", _NAME, s_err)
        end
      end
      server_sock_io:close()
    end
    if it_server.pid_file_name then
      loop_pid_file = io.open(it_server.pid_file_name)
    end
  end
end

_M.httpServer = function(i_callback, it_log, it_env, it_server)
-- no return value
  _M.runServer(i_callback, it_log, it_env, it_server, "http")
end

_M.scgiServer = function(i_callback, it_log, it_env, it_server)
-- no return value
  _M.runServer(i_callback, it_log, it_env, it_server, "scgi")
end

return _M

