protocol = "scgi" -- scgi|http
ip = "127.0.0.1"
port = 5991
--pid_file = "/run/scgi.pid"

--[[
-- if protocol == "http" - define trusted header from proxy
trusted_header = {
  REMOTE_ADDR = true,
  REMOTE_PORT = true,
  SCRIPT_NAME = true,
}
]]

log = {
  ERROR = "/var/log/scgi.error.log",
  WARN = "/var/log/scgi.log",
  INFO = "/var/log/scgi.log",
--  DEBUG = "/var/log/scgi.debug.log",
  ACCESS = "/var/log/scgi.access.log",
}

log = {
  ERROR = "stderr",
  WARN = "stderr",
  INFO = "stdout",
  ACCESS = "stdout",
}

cgi_map = {
  doc_root = "/srv/cgi_lua",
  doc_status = 200,
  doc_type = "cgi", -- cgi|lcgi|text
--  [404] = "404 - Not found",
--  [500] = "500 - Internal Server Error",

-- DEFAULT index
  ["/"] = "/test.cgi",

  ["env.localhost/"] = {"/env", doc_root = "/usr/bin", doc_type = "cgi"},
  ["env.localhost/etex"] = {"/scgi.txt", doc_type = "text"},
  -- remove default for stex on env.localhost
  ["env.localhost/stex"] = "",
  --["env.localhost/stex"] = {},

  ["/test"] = "/test.cgi",
  ["/env"] = {"/env", doc_root = "/usr/bin", doc_type = "cgi"},
  ["/stex"] = {"/scgi.txt", doc_type = "text"},
  ["/lt"] = {"/cgi_test.lua", doc_type = "lcgi"},
  ["/with_path/*"] = "/script_that_interprets_path",
}

