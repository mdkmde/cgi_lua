#!/usr/bin/env lua
if tonumber((string.gsub(_VERSION, "Lua ", ""))) < 5.2 then error("Require Lua 5.2"); end
--[[
https://gitlab.com/mdkmde/cgi_lua/ - matthias (at) koerpermagie.de

Copyright (c) MIT-License
Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
]]

local t_config = {}
local s_usage = string.format("Usage: %s -h|--help|config_file\n", arg[0])
if not(arg[1]) or string.find(arg[1], "-h") then
  io[arg[1] and "stdout" or "stderr"]:write(s_usage)
  os.exit(arg[1] and 0 or 1)
end
local parse_conf = loadfile(arg[1], "t", t_config)
if not(parse_conf) then
  io.stderr:write(string.format("ERROR: Cannot parse config file: %s\n%s", arg[1], s_usage))
  os.exit(1)
end
local b_parse_ok, s_parse_err = pcall(parse_conf)
if not(b_parse_ok) then
  io.stderr:write(string.format("ERROR: %s\n", s_parse_err))
  os.exit(1)
end
local cgi = require("cgi")
local b_islog, flog = pcall(require, "flog")
local nolog = function() end
local log = b_islog and flog.init(t_config.log or {}) or setmetatable({}, {__index = function() return nolog; end})
local tool = require("tool")

local t_valid_doc_type = { cgi = "cgi", lcgi = "lcgi", text = "text" }
local t_valid_doc_status = { [200] = 200, [204] = 204, [301] = 301, [302] = 302, [400] = 400, [401] = 401, [403] = 403 }
local t_cgi_map = {
  [404] = { n_doc_status = 404, s_doc = "404 - Not found" },
  [500] = { n_doc_status = 500, s_doc = "500 - Internal Server Error" },
  s_doc_root = string.gsub(t_config.cgi_map.doc_root, "/*$", ""),
  n_doc_status = t_config.cgi_map.doc_status and t_valid_doc_status[t_config.cgi_map.doc_status] or 200,
  s_doc_type = t_config.cgi_map.doc_type and t_valid_doc_type[string.lower(t_config.cgi_map.doc_type)] or "cgi",
}
local mt_cgi_map = {
  __index = function(it_t, is_k)
    -- s_doc_root, n_doc_status, s_doc_type
    if rawget(t_cgi_map, is_k) then
      return t_cgi_map[is_k]
    else
      return t_cgi_map[404]
    end
  end
}
if t_config.cgi_map.doc_status and not(t_cgi_map.n_doc_status == t_config.cgi_map.doc_status) then
  log.WARN("Invalid doc_status: %i, will return 500", t_config.cgi_map.doc_status)
  t_cgi_map.n_doc_status = 500
end
if t_config.cgi_map.doc_type and not(t_cgi_map.s_doc_type == t_config.cgi_map.doc_type) then
  log.WARN("Invalid doc_type: \"%s\", will return 500", t_config.cgi_map.doc_type)
  t_cgi_map.n_doc_status = 500
end
for s_k, t_v in pairs(t_config.cgi_map) do
  if type(s_k) == "number" or string.find(s_k, "/") then
    t_cgi_map[s_k] = {}
    setmetatable(t_cgi_map, mt_cgi_map)
    if type(t_v) == "string" then
      t_v = { t_v }
    end
    if t_cgi_map[s_k].n_doc_status == 301 or t_cgi_map[s_k].n_doc_status == 302 then
      t_cgi_map[s_k].s_doc = t_v[1]
    elseif not(t_v[1]) or (t_v[1] == "") then
      t_cgi_map[s_k].n_doc_status = 404
      t_cgi_map[s_k].s_doc = t_cgi_map[404].s_doc
    else
      t_cgi_map[s_k].s_doc = string.format("%s/%s", t_config.cgi_map[s_k].doc_root and string.gsub(t_config.cgi_map[s_k].doc_root, "/*$", "") or t_cgi_map.s_doc_root, string.gsub(t_v[1], "^/*", ""))
      local f_content, s_err = io.open(t_cgi_map[s_k].s_doc)
      if not(f_content) then
        t_cgi_map[s_k].s_doc = string.gsub(t_v[1], "^/*", "")
      else
        f_content:close()
      end
    end
    if t_config.cgi_map[s_k].n_doc_status then
      if not(t_valid_doc_status[t_config.cgi_map[s_k].n_doc_status]) then
        log.WARN("Invalid doc_status: %i, will return 500", t_config.cgi_map[s_k].n_doc_status)
        t_cgi_map[s_k].n_doc_status = 500
      end
    elseif type(s_k) == "number" then
      t_cgi_map[s_k].n_doc_status = s_k
    end
    if t_config.cgi_map[s_k].doc_type then
      t_cgi_map[s_k].s_doc_type = t_valid_doc_type[string.lower(t_config.cgi_map[s_k].doc_type)]
      if t_config.cgi_map[s_k].doc_type and not(t_cgi_map[s_k].s_doc_type) then
        log.WARN("Invalid doc_type: \"%s\", will return 500", t_config.cgi_map[s_k].doc_type)
        t_cgi_map[s_k].n_doc_status = 500
      end
    end
  end
end

local do_cgi = function(it_cgi_ctx)
--TODO: doc_env / log per cgi
  local t_server_name = {it_cgi_ctx:getEnv("HTTP_HOST"), ""}
  local s_script_name = ""
  for n_i = 1, 2 do
    local s_env_script_name = string.gsub(string.gsub(it_cgi_ctx:getEnv("SCRIPT_NAME"), "^[/.]*", "/"), "%?.*$", "")
    s_script_name = string.format("%s%s", t_server_name[n_i], s_env_script_name)
    if not(rawget(t_cgi_map, s_script_name)) then
      while not(s_env_script_name == "") do
        s_env_script_name = string.gsub(s_env_script_name, "/[^/]*$", "")
        if rawget(t_cgi_map, string.format("%s%s/*", t_server_name[n_i], s_env_script_name)) then
          s_script_name = string.format("%s%s/*", t_server_name[n_i], s_env_script_name)
          break
        end
      end
    end
    if rawget(t_cgi_map, s_script_name) then
      it_cgi_ctx:updateScriptName(s_env_script_name)
      break
    end
  end
  if t_cgi_map[s_script_name].n_doc_status == 301 or t_cgi_map[s_script_name].n_doc_status == 302 then
    it_cgi_ctx:redirect(t_cgi_map[s_script_name].s_doc, t_cgi_map[s_script_name].n_doc_status)
  elseif t_cgi_map[s_script_name].n_doc_status == 404 then
    it_cgi_ctx:httpError(404, nil, t_cgi_map[s_script_name].s_doc)
  else
    if (t_cgi_map[s_script_name].s_doc_type or t_cgi_map.s_doc_type) == "lcgi" then
      local t_lcgi_env = _ENV
      t_lcgi_env.cgi_ctx = it_cgi_ctx
      loadfile(t_cgi_map[s_script_name].s_doc, "bt", t_lcgi_env)()
    elseif (t_cgi_map[s_script_name].s_doc_type or t_cgi_map.s_doc_type) == "text" then
      local f_content, s_err = io.open(t_cgi_map[s_script_name].s_doc)
      if not(f_content) then
        log.ERROR("Cannot open file: \"%s\"", s_err)
        it_cgi_ctx:httpError(404, nil, t_cgi_map[404].s_doc)
      end
      it_cgi_ctx:write(f_content:read("*a"))
      f_content:close()
    elseif (t_cgi_map[s_script_name].s_doc_type or t_cgi_map.s_doc_type) == "cgi" then
      local env_export = {
        vars = {},
        keys = {},
      }
      for k, v in it_cgi_ctx:getEnv() do
        env_export.vars[#env_export.vars + 1] = string.format("%s=\"%s\";", k, v)
        env_export.keys[#env_export.keys + 1] = k
      end
      env_export.vars[#env_export.vars + 1] = "GATEWAY_INTERFACE=\"CGI/1.1\";"
      env_export.vars[#env_export.vars + 1] = string.format("LCGI_PID=\"%d\";", tool.getPid())
      env_export.string = string.format("%s export %s;", table.concat(env_export.vars), table.concat(env_export.keys, " "))
      local s_content, s_err = io.popen(string.format("%s%s 2>/dev/null", env_export.string, t_cgi_map[s_script_name].s_doc))
      if not(s_content) then
        log.ERROR("CGI execution failed: \"%s\"", s_err)
        it_cgi_ctx:httpError(502, nil, t_cgi_map[502].s_doc)
      end
      local setHeader = function() end
      setHeader = function(is_content)
        local s_l = is_content:read("*l")
        if s_l == "" then
          return
        elseif not(string.find(s_l, ":")) then
          log.WARN("Invalid header: \"%s\"", s_l)
          return s_l
        end
        it_cgi_ctx:setHeader(s_l)
        return setHeader(is_content)
      end
      local s_no_header = setHeader(s_content)
      if (it_cgi_ctx:getEnv("REQUEST_METHOD") == "HEAD") then
        it_cgi_ctx:write("")
      else
        if s_no_header then
          it_cgi_ctx:write(s_no_header)
        end
        it_cgi_ctx:write(s_content:read("*a"))
      end
      s_content:close()
    else
      log.ERROR("Invalid doc_type: \"%s\"", t_cgi_map[s_script_name].s_doc_type or t_cgi_map.s_doc_type)
      it_cgi_ctx:httpError(500, nil, t_cgi_map[500].s_doc)
    end
  end
end

if t_config.pid_file_name then
  local loop_pid_file = io.open(t_config.pid_file_name, "w")
  if not(loop_pid_file) then
    log.ERROR("Cannot write pid file: %s", t_config.pid_file_name)
    io.stderr:write(string.format("Cannot write pid file: %s", t_config.pid_file_name))
    os.exit(1)
  end
  tool.bg()
  loop_pid_file:write(tool.getPid())
  loop_pid_file:close()
end

local tablestring
tablestring = function(is_table_name, it_table, it_ret, ix)
  local t_ret = it_ret or {string.format("%s  = { ", is_table_name)}
  local s_k, v = next(it_table, ix)
  if not(s_k) then
    t_ret[#t_ret] = string.gsub(t_ret[#t_ret], ", $", " }, ")
    return table.concat(t_ret)
  elseif type(v) == "table" then
    t_ret[(#t_ret + 1)] = tablestring(s_k, v)
  else
    t_ret[(#t_ret + 1)] = string.format("%s = %s, ", s_k, v)
  end
  return tablestring(is_table_name, it_table, t_ret, s_k)
end

local s_log_info = t_config.pid_file_name and string.format(" (PID: %s)", t_config.pid_file_name) or ""
if t_config.protocol == "http" then
-- config setting: if protocol == "http" and "standalone" (Recommended: use behind proxy and set env)
  t_config.name = t_config.name or "localhost"
  t_config.software = t_config.software or "lcgi"
  s_log_info = string.format("%s, Name: %s, Software: %s", s_log_info, t_config.name, t_config.software)
end
log.INFO("Start lcgi %s server: %s:%s%s", t_config.protocol, t_config.ip, t_config.port, s_log_info)
if log.DEBUG then
  if t_config.cgi_env then log.DEBUG("CGI Environment: %s", string.gsub(tablestring("cgi_env", t_config.cgi_env), ", $", "")) end
  log.DEBUG("Configuration: %s", string.gsub(tablestring("cgi_map", t_cgi_map), ", $", ""))
end
cgi.runServer(do_cgi, log, t_config.cgi_env, {protocol = t_config.protocol, host = t_config.ip, port = t_config.port, pid_file_name = t_config.pid_file_name, name = t_config.name, software = t_config.software, document_root = t_config.cgi_map.doc_root, trusted_header = t_config.trusted_header})

